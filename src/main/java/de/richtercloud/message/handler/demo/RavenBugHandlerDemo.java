/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.richtercloud.message.handler.demo;

import de.richtercloud.message.handler.ExceptionMessage;
import de.richtercloud.message.handler.raven.bug.handler.RavenBugHandler;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
internal implementation notes:
- Extending JFrame and using this as dialog parent is the easiest way to
properly shutdown AWT threads.
*/
/**
 *
 * @author richter
 */
public class RavenBugHandlerDemo extends JFrame {
    private final static Logger LOGGER = LoggerFactory.getLogger(RavenBugHandlerDemo.class);
    private static final long serialVersionUID = 1L;
    private final JButton button = new JButton("Critical method");

    public RavenBugHandlerDemo() {
        super();
        String tmpDirPath = System.getProperty("java.io.tmpdir");
        assert tmpDirPath != null;
        File credentialStoreFile = new File(tmpDirPath,
                RavenBugHandlerDemo.class.getSimpleName());
            //use the same file for every run in order to allow demonstrating
            //storage of credentials
        LOGGER.info(String.format("using file '%s' for credential store",
                credentialStoreFile.getAbsolutePath()));
        RavenBugHandler ravenBugHandler = new RavenBugHandler("some DSN");
        this.getContentPane().add(button);
        button.addActionListener((event) -> {
            try {
                criticalMethod();
            }catch(MyException ex) {
                ravenBugHandler.handleUnexpectedException(new ExceptionMessage(ex));
            }
        });
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                ravenBugHandler.shutdown();
            }
        });
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setBounds(10, 10, 800, 600);
        this.pack();
    }

    private static void criticalMethod() {
        throw new MyException();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            new RavenBugHandlerDemo().setVisible(true);
        });
    }

    private static class MyException extends RuntimeException {
    }
}
