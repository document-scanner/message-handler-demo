/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.richtercloud.message.handler.demo;

import de.richtercloud.message.handler.BugHandler;
import de.richtercloud.message.handler.ConfirmMessageHandler;
import de.richtercloud.message.handler.DialogBugHandler;
import de.richtercloud.message.handler.DialogConfirmMessageHandler;
import de.richtercloud.message.handler.DialogMessageHandler;
import de.richtercloud.message.handler.ExceptionMessage;
import de.richtercloud.message.handler.Message;
import java.awt.HeadlessException;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

/**
 *
 * @author richter
 */
@SuppressWarnings("PMD.UseUtilityClass")
public class DialogMessageHandlerDemo extends JFrame {
    private static final long serialVersionUID = 1L;

    public DialogMessageHandlerDemo() throws HeadlessException {
        super();
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setBounds(10, 10, 800, 600);
        DialogMessageHandler dialogMessageHandler = new DialogMessageHandler(this);
        dialogMessageHandler.handle(new Message("A very very very very very "
                + "very very very very very very very very very very very long "
                + "text",
                JOptionPane.ERROR_MESSAGE,
                "Title"));
        ConfirmMessageHandler confirmMessageHandler = new DialogConfirmMessageHandler(this);
        confirmMessageHandler.confirm(new Message("A very very very very very "
                + "very very very very very very very very very very very long "
                + "text",
                JOptionPane.ERROR_MESSAGE,
                "Title"));
        confirmMessageHandler.confirm(new Message("A very very very very very "
                + "very very very very very very very very very very very long "
                + "text",
                JOptionPane.ERROR_MESSAGE,
                "Title"),
                "Option 1", "Option 2");
        BugHandler bugHandler = new DialogBugHandler(this,
                "http://example.com" //bugReportingURL
        );
        bugHandler.handleUnexpectedException(new ExceptionMessage(new RuntimeException()));
        bugHandler.shutdown();
        pack();
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            new DialogMessageHandlerDemo().setVisible(true);
        });
    }
}
